from django.contrib import admin

from base.models import Product

# Register your models here.

@admin.register(Product)
class GameAdmin(admin.ModelAdmin):
    list_display = ('name', 'price',)
